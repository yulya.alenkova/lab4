#include "linked_list.h"
#include "ho_functions.h"
#include <stdio.h>
#include <stddef.h>
#include <limits.h>
#include <math.h>


static void print_value_with_space(int x){
	printf("%d ",x);
}

static void print_value_with_newline(int x){
	printf("%d\n",x);
}


static int squareOfNumber(int x){
	if (x*x>INT_MAX) return 0;
	return x*x;
}

static int cubeOfNumber(int x){
	if (x*x*x>INT_MAX || x*x*x<INT_MIN) return 0;
	return x*x*x;
}

static int sum(int a,int b){
	if ((a+b)>INT_MAX || (a+b)<INT_MIN) return 0;
	return a+b;
}

static int maxValue(int a,int b){
	if (a>=b) return a;
	return b;
}

static int minValue(int a,int b){
	if (b<=a) return b;
	return a;
}

int abs(int a);


static int mul2(int a){
	if (a*2>INT_MAX || a*2<INT_MIN) return 0;
	return a*2;
}


int main( void) {
	l_list* array;
	l_list* new_pointer;
	array = create_list();
	printf("Список через пробел\n");
	foreach(array,print_value_with_space);
	printf("\nСписок c переносом строки:\n");
	foreach(array,print_value_with_newline);

	new_pointer=map(squareOfNumber,array);
	puts("\nКвадраты чисел:");
	foreach(new_pointer,print_value_with_space);
	list_free(new_pointer);
	puts("\n\nКубы чисел:");
	new_pointer=map(cubeOfNumber,array);
	foreach(new_pointer,print_value_with_space);
	list_free(new_pointer);
	printf("\n\nСумма элементов списка:%d\n",foldl(0,sum,array));
	printf("\nМаксимальный элемент списка:%d\n",foldl(INT_MIN,maxValue,array));
	printf("\nМинимальный элемент списка:%d\n",foldl(INT_MAX,minValue,array));

	map_mut(abs,array);
	puts("\nМодули элементов:");
	foreach(array,print_value_with_space);

	new_pointer=iterate(2,10,mul2);
	puts("\n\nПервые десять чисел степеней 2:");
	foreach(new_pointer,print_value_with_space);
	list_free(new_pointer);

	puts("\n");

	if(!save(array, "llist.txt")){
        puts("Ошибка при сохранении");
        return 0;
    }
    else
    	puts("Linked list успешно сохранен в файл llist.txt");

    puts("\n");

    new_pointer = load("llist.txt");
    if (new_pointer!=NULL){
    	puts("Файл успешно прочитан");
    	foreach(new_pointer,print_value_with_space);
    }
    else {
    puts("Ошибка при чтении");
    return 0;
	}

	puts("\n");

    if (!serialize(array, "llist.bin")) {
            puts("Ошибка при сериализации");
            return 0;
    }
    else
    	puts("Сериализация прошла успешно");

    puts("\n");

	list_free(new_pointer);

	new_pointer = deserialize("llist.bin");
    if (new_pointer!=NULL){
    	puts("Десериализвция прошла успешно");
    	foreach(new_pointer,print_value_with_space);
    }
    else {
    puts("Ошибка при Десериализвция");
    return 0;
	}

	list_free(new_pointer);
	list_free(array);
	return 0;
}
