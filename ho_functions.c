#include <malloc.h>
#include "linked_list.h"
#include "ho_functions.h"

void foreach(l_list* head, void (*func)(int)){
	size_t i;
	size_t length=list_length(head);
	for (i=0;i<length;i++){
		func(list_get(head,i));
	}
}

l_list* map(int (*func)(int), l_list* head){
	size_t i;
	size_t length=list_length(head);
	l_list* new_pointer=list_add_front(NULL,func(list_get(head,0)));
	for (i=1;i<length;i++){
		list_add_back(new_pointer,func(list_get(head,i)));
	}
	return new_pointer;
}

void map_mut(int (*func)(int), l_list* head){
	size_t i;
	size_t length=list_length(head);
	for (i=0;i<length;i++){
		list_change_value(head,i,func(list_get(head,i)));
	}
}

int foldl(int rax,int (*func)(int,int),l_list* head){
	size_t i;
	size_t length=list_length(head);
	for (i=0;i<length;i++){
		rax=func(rax,list_get(head,i));
	}
	return rax;
}

l_list* iterate(int s,int n,int (*func)(int)){
	size_t i;
	l_list* new_pointer=list_add_front(NULL,s);
	for(i=1;i<n;i++){
		s=func(s);
		list_add_back(new_pointer,s);
	}
	return new_pointer;
}
