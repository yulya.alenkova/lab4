#ifndef LINKED_LIST
#define LINKED_LIST
#include <stddef.h>

typedef struct list l_list;

l_list* list_add_front(l_list* head, int new_value);
void print_list(l_list* head);
l_list* create_list();
void list_add_back(l_list* back, int new_value);
int list_length(l_list* head);
int list_sum(l_list* head);
l_list* list_node_at(l_list* head, size_t index);
int list_get(l_list* head, size_t index);
void list_free(l_list* head);
void list_change_value(l_list* head,size_t i,int value);
int save(l_list* list,const char* filename);
l_list* load(const char* filename);
int serialize(l_list* list, const char * filename);
l_list* deserialize(const char* filename);
#endif