#include <stdio.h>
#include <malloc.h>

typedef struct list {
	int value;
	struct list* next;
}l_list;

l_list* list_add_front(l_list* head, int new_value){
	l_list* new_elem = (l_list*) malloc(sizeof(l_list));

	new_elem->value = new_value;
	new_elem->next = head;

	return new_elem;

}

l_list* create_list() {
	l_list* head = NULL;
	size_t i;
	size_t cnt;
	puts("The size of list is ");
	scanf("%zu", &cnt );
	int value;
	for (i = 0; i < cnt; i ++) {
		printf("The %lu element is ", (cnt - i-1) );
		scanf("%d", &value);
		head = list_add_front(head, value);
	}
	return head;
}

void print_list(l_list* head) {
	l_list* current = head;

	while (current != NULL) {
		printf("%d ", current->value);
		current = current->next;
	}
}

void list_add_back(l_list* head, int new_value) {
	l_list* new_elem = (l_list*) malloc(sizeof(l_list));
	l_list* current = head;
	
	new_elem->value = new_value;
	new_elem->next = NULL;

	while (current->next != NULL){
		current = current->next;
	}
	current->next = new_elem;

}

int list_length(l_list* head) {
	int length = 0; 
	l_list* current = head;

	while (current != NULL){
		current = current->next;
		length ++;
	}
	return length;
}

int list_sum(l_list* head) {
	int sum = 0;
	l_list* current = head;

	while(current != NULL){
		sum += current->value;
		current = current->next;
	}
	return sum;
}

l_list* list_node_at(l_list* head, size_t index){
	size_t current_index = 0;
	l_list* current = head;

	while (current != NULL && current_index != index){
		current_index ++;
		current = current->next;
	}
	if (index == current_index)
	       	return current;
	else
		return NULL;
}

int list_get(l_list* head, size_t index){
	size_t current_index = 0;
	l_list* current = list_node_at(head, index);
	if (current == NULL) return 0;
	else return current->value;
}

void list_free(l_list* head) {
	while (head != NULL){
		l_list* next = head->next;
		free(head);
		head = next;
	}
}

void list_change_value(l_list* head,size_t i,int value){
	l_list* node=list_node_at(head,i);
	node->value=value;
}

int save(l_list* list,const char* filename){
    l_list* iter;
    FILE* f = fopen(filename, "w");

    for (iter = list; iter != NULL; iter = iter->next){
        int err = fprintf(f, "%d ", iter->value);
        if(err < 0) {
            fclose(f);
            return 0;
        }
    }
    fclose(f);
    return 1;
}


l_list* load(const char* filename){
	FILE* file = fopen(filename, "r");
	int value;
	l_list* pointer=NULL;
	if (file==NULL){
		puts("Файл не найден");
		return NULL;
	}
	while(fscanf(file,"%d",&value)!=EOF){
		if (pointer==NULL) pointer=list_add_front(NULL,value);
		else list_add_back(pointer,value);
	}
	return pointer;
}


int serialize(l_list* list, const char * filename){
    l_list* iter;
    FILE* f = fopen(filename, "wb");
    for (iter = list; iter != NULL; iter = iter->next){
        fwrite(&(iter->value), sizeof(int),1, f);
        if(ferror(f)){
            fclose(f);
            return 0;
        }
    }
    fclose(f);
    return 1;
}

l_list* deserialize(const char* filename){
	FILE* file = fopen(filename, "rb");
	int value;
	l_list* pointer=NULL;
	if (file==NULL){
		puts("Файл не найден");
		return NULL;
	}


	while (1){
        fread(&value,  sizeof(int),1, file);
        if(feof(file)) return pointer;

        if(ferror(file)){
            fclose(file);
            return 0;
        }
        if (pointer==NULL) pointer=list_add_front(NULL,value);
		else list_add_back(pointer,value);
	}
    
	return pointer;
}







